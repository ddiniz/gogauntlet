package main

import (
	mysdl "gogauntlet/source/sdl"
	"math/rand"
	"time"
)

//TODO: implement some type of pallete function to change tile colors
//TODO: GUI
//TODO: Main menu
//TODO: Map builder menu and interface
//TODO: save and load maps
//TODO: player character
//TODO: player animations
//TODO: player movement
//TODO: projectile and projectile movement
//TODO: collisions

func main() {
	rand.Seed(time.Now().UnixNano())
	mysdl.RunGame()
}
