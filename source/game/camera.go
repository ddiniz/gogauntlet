package game

import (
	in "gogauntlet/source/input"
	u "gogauntlet/source/utils"
)

type Camera struct {
	Pos   u.Vector2[int32]
	Speed u.Vector2[int32]
}

func NewCamera() *Camera {
	return &Camera{
		Pos:   u.Vector2[int32]{X: 0, Y: 0},
		Speed: u.Vector2[int32]{X: 32, Y: 32},
	}
}

func (camera *Camera) HandleInput(input *in.GameInput) {
	if input.Actions[in.LEFT] {

	}
}
