package game

import (
	"gogauntlet/source/gui"
	in "gogauntlet/source/input"
	spr "gogauntlet/source/sprite"
	u "gogauntlet/source/utils"
	"log"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	SPRITE_WIDTH  = int32(16)
	SPRITE_HEIGHT = int32(16)
	SPRITE_SCALE  = int32(2)
	MAP_SIZE      = int32(32)
)

type Game struct {
	Floors            [][]*spr.Sprite `json:"floors"`
	Walls             [][]*spr.Sprite `json:"walls"`
	Map               Map             `json:"map"`
	State             GameStateEnum   `json:"state"`
	Camera            *Camera         `json:"camera"`
	Gui               *gui.Gui        `json:"gui"`
	CurrentWallBrush  int
	CurrentFloorBrush int
}

func NewGame(width, height int) *Game {
	return &Game{
		Floors: make([][]*spr.Sprite, 0),
		Walls:  make([][]*spr.Sprite, 0),
		Map: Map{
			Tiles: make([][]any, MAP_SIZE),
			Cols:  int32(MAP_SIZE),
			Rows:  int32(MAP_SIZE),
		},
		State:             INITIALIZATION,
		CurrentWallBrush:  0,
		CurrentFloorBrush: 4,
	}
}

func GetSprites(textures map[string]*sdl.Texture, name string) []*spr.Sprite {
	return spr.ExtractSpritesFromSpritesheet(textures[name], name, SPRITE_WIDTH, SPRITE_HEIGHT, SPRITE_SCALE)
}

func (game *Game) InitializeGame(textures map[string]*sdl.Texture) {
	game.InitializeSprites(textures)
	GenerateFilledMap(&game.Map, game.Walls[0])
	AutotileEntireMap(&game.Map)
}

func (game *Game) InitializeSprites(textures map[string]*sdl.Texture) {
	game.Floors = append(game.Floors, GetSprites(textures, "floor_special_0"))
	game.Floors = append(game.Floors, GetSprites(textures, "floor_special_1"))
	game.Floors = append(game.Floors, GetSprites(textures, "floor_special_2"))
	game.Floors = append(game.Floors, GetSprites(textures, "floor_special_3"))
	game.Floors = append(game.Floors, GetSprites(textures, "floor0"))
	game.Floors = append(game.Floors, GetSprites(textures, "floor1"))
	game.Floors = append(game.Floors, GetSprites(textures, "floor2"))
	game.Floors = append(game.Floors, GetSprites(textures, "floor3"))
	game.Floors = append(game.Floors, GetSprites(textures, "floor4"))
	game.Floors = append(game.Floors, GetSprites(textures, "floor5"))
	game.Walls = append(game.Walls, GetSprites(textures, "wall0"))
	game.Walls = append(game.Walls, GetSprites(textures, "wall1"))
	game.Walls = append(game.Walls, GetSprites(textures, "wall2"))
	game.Walls = append(game.Walls, GetSprites(textures, "wall3"))
	game.Walls = append(game.Walls, GetSprites(textures, "wall4"))
	game.Walls = append(game.Walls, GetSprites(textures, "wall5"))
}

func (game *Game) UpdateObjectsPositions(mousePos u.Vector2[int32]) {

}

func (game *Game) HandleClicking(mousePos u.Vector2[int32], sprites []*spr.Sprite, addFloor bool) bool {
	return ClickedMap(&game.Map, mousePos, sprites, addFloor)
}

func (game *Game) IsGameEnded() bool {
	return false
}

var NUM = uint8(0)

func (game *Game) HandleInput(input *in.GameInput) GameStateEnum {
	if in.Input.Actions[in.SAVE] {
		log.Println("SAVED GAME")
		GameToFile(game, "savedstate")
	}
	if in.Input.Actions[in.LOAD] {
		log.Println("LOADED GAME")
		*game = *FileToGame("savedstate")
	}
	if in.Input.Actions[in.RESET] {
		GenerateTestTileMap(&game.Map, game.Walls[0], game.Floors[4], NUM, 1)
		GetFloorTileShadowIndex(&game.Map, 2, 2)
		AutotileEntireMap(&game.Map)
		NUM++
	}
	return INPUT
}
