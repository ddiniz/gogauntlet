package game

import u "gogauntlet/source/utils"

type GameStateEnum uint8

const (
	INITIALIZATION GameStateEnum = iota
	INPUT
	RECALCULATE_BOARD
	END
	RESTART
)

func (game *Game) ChangeState(next GameStateEnum) {
	switch game.State {
	case INITIALIZATION:
		switch next {
		case INPUT:
			game.State = INPUT
		default:
			u.PrintBadState(game.State, next)
		}
	case INPUT:
		switch next {
		case RECALCULATE_BOARD:
			game.State = RECALCULATE_BOARD
		case RESTART:
			game.State = RESTART
		default:
			u.PrintBadState(game.State, next)
		}
	case RECALCULATE_BOARD:
		switch next {
		case INPUT:
			game.State = INPUT
		case END:
			game.State = END
		default:
			u.PrintBadState(game.State, next)
		}
	case END:
		switch next {
		case END:
			game.State = END
		default:
			u.PrintBadState(game.State, next)
		}
	case RESTART:
		switch next {
		case INITIALIZATION:
			game.State = INITIALIZATION
		default:
			u.PrintBadState(game.State, next)
		}
	}
}
