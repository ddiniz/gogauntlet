package game

import (
	spr "gogauntlet/source/sprite"
	t "gogauntlet/source/tile"
	u "gogauntlet/source/utils"
)

type Map struct {
	Tiles [][]any `json:"tiles"`
	Cols  int32   `json:"cols"`
	Rows  int32   `json:"rows"`
}

func GenerateFilledMap(gamemap *Map, wallSprites []*spr.Sprite) {
	for i := int32(0); i < gamemap.Rows; i++ {
		gamemap.Tiles[i] = make([]any, gamemap.Rows)
		for j := int32(0); j < gamemap.Cols; j++ {
			gamemap.Tiles[i][j] = t.NewWallTile(
				wallSprites,
				t.W_ENCLOSED,
				false,
				u.Vector2[int32]{X: i, Y: j},
			)
		}
	}
}

func GenerateTestTileMap(
	gamemap *Map,
	wallSprites []*spr.Sprite,
	floorSprites []*spr.Sprite,
	num uint8,
	center uint8,
) {
	testMap := t.NumberToMap(num, center)
	gamemap.Rows = int32(len(testMap))
	gamemap.Cols = int32(len(testMap[0]))
	gamemap.Tiles = make([][]any, gamemap.Cols)
	for i := int32(0); i < gamemap.Rows; i++ {
		gamemap.Tiles[i] = make([]any, gamemap.Rows)
		for j := int32(0); j < gamemap.Cols; j++ {
			var tile any
			tile = t.NewWallTile(
				wallSprites,
				t.W_ENCLOSED,
				false,
				u.Vector2[int32]{X: i, Y: j},
			)
			if testMap[i][j] == 0 {
				tile = t.NewFloorTile(
					floorSprites,
					t.F_NO_SHADOW,
					0,
					u.Vector2[int32]{X: int32(i), Y: int32(j)},
				)
			}
			gamemap.Tiles[i][j] = tile
		}
	}
}

func AutotileEntireMap(gamemap *Map) {
	for i := int32(0); i < gamemap.Cols; i++ {
		for j := int32(0); j < gamemap.Rows; j++ {
			UpdateWallOrFloorSprite(gamemap, i, j)

		}
	}
}

func UpdateWallOrFloorSprite(gamemap *Map, i, j int32) {
	cell := gamemap.Tiles[i][j]
	if cell == nil {
		return
	}
	wall, ok := cell.(*t.WallTile)
	if ok {
		wall.SpriteIndex = GetWallTileSpriteIndex(gamemap, i, j)
		return
	}
	floor, ok := cell.(*t.FloorTile)
	if ok {
		floor.ShadowIndex = GetFloorTileShadowIndex(gamemap, i, j)
		return
	}
}

func GetWallTileSpriteIndex(gamemap *Map, i, j int32) t.WallTileEnum {
	num := uint8(0)
	for k := 0; k < len(t.DIRECTIONS); k++ {
		posX := i + t.DIRECTIONS[k].X
		posY := j + t.DIRECTIONS[k].Y
		valid := u.CheckBound(posX, 0, gamemap.Rows) && u.CheckBound(posY, 0, gamemap.Cols)
		if valid {
			tile := gamemap.Tiles[posX][posY]
			if tile == nil {
				continue
			}
			_, ok := tile.(*t.WallTile)
			if !ok {
				continue
			}
		}
		num |= t.BITS[k]
	}
	return t.AUTOTILE_WALL[num]
}

func GetFloorTileShadowIndex(gamemap *Map, i, j int32) t.FloorTileShadowEnum {
	num := uint8(0)
	for k := 0; k < len(t.DIRECTIONS); k++ {
		posX := i + t.DIRECTIONS[k].X
		posY := j + t.DIRECTIONS[k].Y
		valid := u.CheckBound(posX, 0, gamemap.Rows) && u.CheckBound(posY, 0, gamemap.Cols)
		if valid {
			tile := gamemap.Tiles[posX][posY]
			if tile == nil {
				continue
			}
			_, ok := tile.(*t.WallTile)
			if !ok {
				continue
			}
		}
		num |= t.BITS[k]
	}
	return t.AUTOTILE_FLOOR[num]
}

type InHitboxable interface {
	InHitbox(pos u.Vector2[int32]) bool
}

func ClickedMap(
	gamemap *Map,
	mousePos u.Vector2[int32],
	sprites []*spr.Sprite,
	addFloor bool,
) bool {
	//TODO: optimize this
	for i := int32(0); i < gamemap.Rows; i++ {
		for j := int32(0); j < gamemap.Cols; j++ {
			cell := gamemap.Tiles[i][j]
			if cell == nil {
				continue
			}

			wallOrFloor, ok := cell.(InHitboxable)
			if !ok || wallOrFloor == nil {
				continue
			}
			if wallOrFloor.InHitbox(mousePos) {
				if addFloor {
					gamemap.Tiles[i][j] = t.NewFloorTile(
						sprites,
						t.F_NO_SHADOW,
						0,
						u.Vector2[int32]{X: int32(i), Y: int32(j)},
					)
					gamemap.Tiles[i][j].(*t.FloorTile).ShadowIndex = GetFloorTileShadowIndex(gamemap, i, j)
					AutotileAroundTile(gamemap, i, j)
					return true
				} else {
					gamemap.Tiles[i][j] = t.NewWallTile(
						sprites,
						t.W_ENCLOSED,
						false,
						u.Vector2[int32]{X: int32(i), Y: int32(j)},
					)
					gamemap.Tiles[i][j].(*t.WallTile).SpriteIndex = GetWallTileSpriteIndex(gamemap, i, j)
					AutotileAroundTile(gamemap, i, j)
					return true
				}
			}
		}
	}
	return false
}

func AutotileAroundTile(gamemap *Map, i, j int32) {
	for k := 0; k < len(t.DIRECTIONS); k++ {
		posX := i + t.DIRECTIONS[k].X
		posY := j + t.DIRECTIONS[k].Y
		valid := u.CheckBound(posX, 0, gamemap.Rows) && u.CheckBound(posY, 0, gamemap.Cols)
		if valid {
			UpdateWallOrFloorSprite(gamemap, posX, posY)
		}
	}
}
