package game

import (
	"encoding/json"
	"fmt"
	u "gogauntlet/source/utils"
	"os"
)

func GameToFile(game *Game, filename string) {
	folder := u.GetGameRootFolder("/")
	readFile, err := os.Create(folder + filename + ".json")
	if err != nil {
		fmt.Println(err)
	}
	json, err := json.Marshal(game)
	if err != nil {
		fmt.Println(err)
	}
	readFile.WriteString(string(json))
	readFile.Close()
}

func FileToGame(filename string) *Game {
	folder := u.GetGameRootFolder("/")
	dat, err := os.ReadFile(folder + filename)
	if err != nil {
		fmt.Println(err)
	}
	game := &Game{}
	json.Unmarshal(dat, game)
	return game
}
