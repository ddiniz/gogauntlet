package input

import (
	u "gogauntlet/source/utils"
)

type InputEnum uint16

const (
	MOUSE_LEFT_DOWN InputEnum = iota
	MOUSE_MIDDLE_DOWN
	MOUSE_RIGHT_DOWN
	MOUSE_LEFT_HOLD
	MOUSE_MIDDLE_HOLD
	MOUSE_RIGHT_HOLD
	MOUSE_LEFT_UP
	MOUSE_MIDDLE_UP
	MOUSE_RIGHT_UP
	KEY_1
	KEY_2
	KEY_3
	KEY_4
	KEY_5
	KEY_6
	KEY_7
	KEY_8
	KEY_9
	KEY_0
	LCTRL
	QUIT
	LEFT
	RIGHT
	UP
	DOWN
	RESET
	SAVE
	LOAD
	INVALID
)

type GameInput struct {
	MousePos u.Vector2[int32]
	Actions  map[InputEnum]bool
}

var Input = &GameInput{
	MousePos: u.Vector2[int32]{X: 0, Y: 0},
	Actions:  make(map[InputEnum]bool, 255),
}

var keys = []InputEnum{KEY_1, KEY_2, KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_0}

func (in *GameInput) PressedNumberRow(current int) (bool, int) {
	for i := 0; i < len(keys); i++ {
		if in.Actions[keys[i]] {
			return true, i
		}
	}
	return false, current
}

func (in *GameInput) PressedNumberRowWithModifier(current int) (bool, int) {
	if !in.Actions[LCTRL] {
		return false, current
	}
	for i := 0; i < len(keys); i++ {
		if in.Actions[keys[i]] {
			return true, i
		}
	}
	return false, current
}

func (i *GameInput) ClickedAnyMouseButton() bool {
	return i.Actions[MOUSE_LEFT_DOWN] ||
		i.Actions[MOUSE_MIDDLE_DOWN] ||
		i.Actions[MOUSE_RIGHT_DOWN] ||
		i.Actions[MOUSE_LEFT_HOLD] ||
		i.Actions[MOUSE_MIDDLE_HOLD] ||
		i.Actions[MOUSE_RIGHT_HOLD] ||
		i.Actions[MOUSE_LEFT_UP] ||
		i.Actions[MOUSE_MIDDLE_UP] ||
		i.Actions[MOUSE_RIGHT_UP]
}

func (input *GameInput) ResetInput() {
	for i := range input.Actions {
		if i == MOUSE_LEFT_HOLD || i == MOUSE_MIDDLE_HOLD || i == MOUSE_RIGHT_HOLD {
			continue
		}
		input.Actions[i] = false
	}
}
