package sdl

import (
	g "gogauntlet/source/game"
	in "gogauntlet/source/input"
	u "gogauntlet/source/utils"
	"log"
)

func GameLoop(game *g.Game) {
	switch game.State {
	case g.INITIALIZATION:
		log.Println("INITIALIZATION")
		game.ChangeState(g.INPUT)
	case g.INPUT:
		newState := game.HandleInput(in.Input)

		if in.Input.ClickedAnyMouseButton() { //intercepts any clicking done on interface first
			if game.Gui != nil && game.Gui.HandleClicking(in.Input.MousePos) {
				break
			}
		}

		pressedNumRow, key1 := in.Input.PressedNumberRow(game.CurrentWallBrush)
		pressedNumRowMod, key2 := in.Input.PressedNumberRowWithModifier(game.CurrentFloorBrush)
		if pressedNumRowMod {
			game.CurrentFloorBrush = u.ClampMax(int(key2), len(game.Floors)-1)
		} else if pressedNumRow {
			game.CurrentWallBrush = u.ClampMax(int(key1), len(game.Walls)-1)
		}
		if in.Input.Actions[in.MOUSE_LEFT_HOLD] {
			if game.HandleClicking(in.Input.MousePos, game.Floors[game.CurrentFloorBrush], true) {
				break
			}
		} else if in.Input.Actions[in.MOUSE_RIGHT_HOLD] {
			if game.HandleClicking(in.Input.MousePos, game.Walls[game.CurrentWallBrush], false) {
				break
			}
		}

		if newState != game.State {
			game.ChangeState(newState)
		}

	case g.RECALCULATE_BOARD:
		log.Println("RECALCULATE_BOARD")
		ended := game.IsGameEnded()
		if ended {
			game.ChangeState(g.END)
			break
		}
		game.ChangeState(g.INPUT)
	case g.END:
		log.Println("END")
		game.ChangeState(g.RESTART)
	case g.RESTART:
		log.Println("RESTART")
		game.ChangeState(g.INITIALIZATION)
	}
}
