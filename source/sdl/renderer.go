package sdl

import (
	g "gogauntlet/source/game"
	gui "gogauntlet/source/gui"

	"github.com/veandco/go-sdl2/sdl"
)

type Renderable interface {
	//Render(sdlRenderer *sdl.Renderer, offset u.Vector2[int32])
	Render(sdlRenderer *sdl.Renderer)
}

func RenderGame(sdlRenderer *sdl.Renderer, game *g.Game, camera *g.Camera) {
	for _, mapRow := range game.Map.Tiles {
		for _, cell := range mapRow {
			if cell == nil {
				continue
			}
			renderable := cell.(Renderable)
			if renderable != nil {
				renderable.Render(sdlRenderer)
			}
		}
	}
}

func RenderGui(sdlRenderer *sdl.Renderer, gui *gui.Gui) {
	gui.Render(sdlRenderer)
}
