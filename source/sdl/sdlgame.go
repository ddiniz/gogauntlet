package sdl

import (
	"fmt"
	g "gogauntlet/source/game"
	in "gogauntlet/source/input"
	"os"

	"github.com/veandco/go-sdl2/sdl"
)

const (
	WINDOW_HEIGHT = int32(1060)
	WINDOW_WIDTH  = int32(768)
)

func RunGame() {
	window, sdlRenderer, _ := initializeSDL("GoGauntlet", WINDOW_HEIGHT, WINDOW_WIDTH)
	defer sdl.Quit()
	defer window.Destroy()
	defer sdlRenderer.Destroy()
	images := loadImages()
	textures := loadTextures(images, sdlRenderer)
	defer unloadImages(images)
	defer unloadTextures(textures)

	sdl.JoystickEventState(sdl.ENABLE)
	camera := g.NewCamera()
	// gui := gui.NewGUI(
	// 	u.Vector2[int32]{X: 100, Y: 100},
	// 	u.Vector2[int32]{X: 32, Y: 32},
	// 	u.Color[uint8]{R: 0, G: 0, B: 0, A: 255},
	// )
	game := g.NewGame(8, 8)
	game.InitializeGame(textures)
	game.Camera = camera
	//game.Gui = gui

	for !in.Input.Actions[in.QUIT] {
		sdlRenderer.Clear()
		sdlRenderer.SetDrawColor(255, 255, 255, 255)
		sdlRenderer.FillRect(&sdl.Rect{X: 0, Y: 0, W: WINDOW_HEIGHT, H: WINDOW_WIDTH})

		GetInput()
		GameLoop(game)
		RenderGame(sdlRenderer, game, camera)
		//RenderGui(sdlRenderer, gui)

		sdlRenderer.Present()
		sdl.Delay(16)
	}
}

func initializeSDL(
	winTitle string,
	winWidth int32,
	winHeight int32,
) (*sdl.Window, *sdl.Renderer, error) {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	window, err := sdl.CreateWindow(winTitle, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		winWidth, winHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
		return nil, nil, err
	}

	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		return nil, nil, err
	}
	return window, renderer, err
}
