package tile

import (
	h "gogauntlet/source/hitbox"
	spr "gogauntlet/source/sprite"
	u "gogauntlet/source/utils"

	"github.com/veandco/go-sdl2/sdl"
)

type FloorTileShadowEnum uint8

const (
	F_NO_SHADOW FloorTileShadowEnum = iota
	F_INNER_CORNER
	F_BOTTOM
	F_OUTER_CORNER
	F_LEFT
	F_DIAGONAL_BOTTOM
	F_DIAGONAL_LEFT
	F_DIAGONAL_CORNER
)

type FloorTile struct {
	Sprite         []*spr.Sprite       `json:"sprite"`
	ShadowIndex    FloorTileShadowEnum `json:"shadowIndex"`
	VariantIndex   int                 `json:"variantIndex"`
	Hitbox         *h.Hitbox           `json:"hitbox"`
	Position       u.Vector2[int32]    `json:"position"`
	CameraPosition u.Vector2[int32]    `json:"cameraPosition"`
}

func NewFloorTile(
	sprite []*spr.Sprite,
	shadowIndex FloorTileShadowEnum,
	variantIndex int,
	position u.Vector2[int32],
) *FloorTile {
	floor := &FloorTile{
		Sprite:       sprite,
		ShadowIndex:  shadowIndex,
		VariantIndex: variantIndex,
	}
	if len(sprite) > 0 {
		width := sprite[0].Scale * sprite[0].TextureRect.W
		height := sprite[0].Scale * sprite[0].TextureRect.H
		floor.Position = u.Vector2[int32]{
			X: width * position.X,
			Y: height * position.Y,
		}
		floor.CameraPosition = position
		floor.Hitbox = &h.Hitbox{
			X: floor.Position.X,
			Y: floor.Position.Y,
			W: width,
			H: height,
		}
	}
	return floor
}

func (tile *FloorTile) Render(sdlRenderer *sdl.Renderer) {
	tile.Sprite[tile.VariantIndex+int(tile.ShadowIndex)].RenderAt(sdlRenderer, &tile.Position)
}

func (tile *FloorTile) InHitbox(pos u.Vector2[int32]) bool {
	return tile.Hitbox.InHitbox(pos)
}
