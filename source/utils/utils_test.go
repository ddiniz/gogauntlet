package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetLast(t *testing.T) {
	array := make([]*int, 0)
	i := (*int)(nil)
	last := GetLast(array)
	assert.Equal(t, i, last)

	j := 0
	array = append(array, &j)
	last = GetLast(array)
	assert.Equal(t, &j, last)

	k := 1
	l := 2
	m := 3
	array = append(array, &k)
	array = append(array, &l)
	array = append(array, &m)
	last = GetLast(array)
	assert.Equal(t, &m, last)
}
